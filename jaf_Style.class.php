<?php

/**
 * @file
 * Contains the class for jaf_Style
 * A small object for holding style information
 */
class jaf_Style {
  /**
   * Font object
   *
   * @var jaf_TrueTypeFontInfo
   */
  public $font = '';

  /**
   * Style identifier
   *
   * @var string
   */
  private $id = null;

  /**
   * Font file name
   *
   * @var string
   */
  private $font_file = '';

  /**
   * Font size in points (pt)
   *
   * @var integer
   */
  private $font_size = 0;

  /**
   * Fore ground color in HTML hexidecimal form
   *
   * @var string
   */
  private $fg_color = '';

  /**
   * Background color in HTML hexidecimal form
   *
   * @var string
   */
  private $bg_color = '';
  
  /**
   * A string of css selectors i.e. h1, div.title, body#terms.title, h1.top etc
   *
   * @var string
   */
  private $css_selectors = '';
  
  /**
   * @return string
   */
  public function getBgColor() {
    return $this->bg_color;
  }

  /**
   * @return string
   */
  public function getCssSelectors() {
    return $this->css_selectors;
  }

  /**
   * @return string
   */
  public function getFgColor() {
    return $this->fg_color;
  }

  /**
   * @return string
   */
  public function getFontFile() {
    return $this->font_file;
  }

  /**
   * @return titleimageTrueTypeFontInfo
   */
  public function getFont() {
    return $this->font;
  }

  /**
   * @return integer
   */
  public function getFontSize() {
    return $this->font_size;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }
  
  /**
   * Returns all style values as an array NOTE: This excludes the font object
   *
   * @return array
   */
  public function getValuesAsArray() {
    return array(
      JAF_STYLE_IDENTIFIER      => $this->getId(),
      JAF_STYLE_FONT_SIZE       => $this->getFontSize(),
      JAF_STYLE_FONT_FILE       => $this->getFontFile(),
      JAF_STYLE_FG_COLOR        => $this->getFgColor(),
      JAF_STYLE_BG_COLOR        => $this->getBgColor(),
      JAF_STYLE_CSS_SELECTORS   => $this->getCssSelectors(),
    );
  }

  /**
   * @param string $bg_color
   */
  public function setBgColor($bg_color) {
    $this->bg_color = $bg_color;
  }

  /**
   * @param string $css_selectors
   */
  public function setCssSelectors($css_selectors) {
    $this->css_selectors = $css_selectors;
  }

  /**
   * @param string $fg_color
   */
  public function setFgColor($fg_color) {
    $this->fg_color = $fg_color;
  }

  /**
   * @param titleimageTrueTypeFontInfo $font
   */
  public function setFont($font) {
    $this->font = $font;
  }

  /**
   * @param string $font_file
   */
  public function setFontFile($font_file) {
    $this->font_file = $font_file;
  }

  /**
   * @param integer $font_size
   */
  public function setFontSize($font_size) {
    $this->font_size = $font_size;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }
  
  /**
   * Constructor
   *
   * @param string  $id
   * @param string  $fontfile
   * @param integer $font_size
   * @param string  $fg_color
   * @param string  $bg_color
   * @param string  $css_selector
   * @return titleimageStyle
   */
  public function jaf_Style($values = array()) {
    if (empty($values)) {
      $values = array(
         JAF_STYLE_IDENTIFIER      => null,
         JAF_STYLE_FONT            => null,
         JAF_STYLE_FONT_SIZE       => 12,
         JAF_STYLE_FONT_FILE       => null,
         JAF_STYLE_FG_COLOR        => '#000000',
         JAF_STYLE_BG_COLOR        => '#FFFFFF',
         JAF_STYLE_CSS_SELECTORS   => 'h1.title');
    }
    
    if (!is_null($values[JAF_STYLE_IDENTIFIER])) {
      $this->setId($values[JAF_STYLE_IDENTIFIER]);
    }
    if (!is_null($values[JAF_STYLE_FONT])) {
      $this->setFont($values[JAF_STYLE_FONT]);
    }
    if (!is_null($values[JAF_STYLE_FONT_FILE])) {
      $this->setFontFile($values[JAF_STYLE_FONT_FILE]);
    }
    if (!is_null($values[JAF_STYLE_FONT_SIZE])) {
      $this->setFontSize($values[JAF_STYLE_FONT_SIZE]);
    }
    if (!is_null($values[JAF_STYLE_FG_COLOR])) {
      $this->setFgColor($values[JAF_STYLE_FG_COLOR]);
    }
    if (!is_null($values[JAF_STYLE_BG_COLOR])) {
      $this->setBgColor($values[JAF_STYLE_BG_COLOR]);
    }
    if (!is_null($values[JAF_STYLE_CSS_SELECTORS])) {
      $this->setCssSelectors($values[JAF_STYLE_CSS_SELECTORS]);
    }
  }
}
