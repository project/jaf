<?php

/**
 * @file
 * Administration include for the jaf module
 */




/**
 * Returns the delete confirmation form.
 */
function jaf_admin_form_delete_style_confirm($form_state, $identifier = false) {
  if (is_null($identifier) || is_null(jaf_get_style($identifier))) {
    drupal_set_message(t('
       An invalid identifier (%identifier) was passed or the associated style 
       was not found.', array('%identifier' => $identifier )), 'error');
    return false;
  }
  
  $form = array(
    '#type'          => 'fieldset',
    '#title'         => t('Confirm delete'),
    '#description'   => t('You are about to delete the style %style', array( '%style' => $identifier)),
    '#submit'        => array('jaf_admin_form_delete_style_confirm_submit'),
    '#validate'      => array('jaf_admin_form_delete_style_confirm_validate'),
    'submit'         => array('#type' => 'submit', '#value' => t('Delete')),
    'cancel'         => array('#value' => l(t('Cancel'), JAF_URI_ADMIN_BASE)),
  );
  
  $form[JAF_STYLE_IDENTIFIER] = array(
    '#type' => 'value',
    '#value' => t($identifier),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Delete'));
  $form['cancel'] = array('#value' => l(t('Cancel'), JAF_URI_ADMIN_BASE));
  
  return $form;
}




/**
 * Submit for the delete form
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function jaf_admin_form_delete_style_confirm_submit($form, &$form_state) {
  $form_state['values'][JAF_STYLE_IDENTIFIER];
  $old_styles = jaf_get_style_list();
  $new_styles = array();
  
  foreach($old_styles as $style) {
    if ($style->getId() != $form_state['values'][JAF_STYLE_IDENTIFIER]) {
      array_push($new_styles, $style);
    }
  }
  
  jaf_set_style_list($new_styles);
  
  // Reset the form values
  $form_state['values'] = array();
  $form_state['redirect'] = JAF_URI_ADMIN_BASE;
  
  drupal_set_message('Style has been deleted.', 'status');
}




/**
 * Validate form deletion values
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function jaf_admin_form_delete_style_confirm_validate(&$form, $form_state) {
  if (is_null(jaf_get_style($form_state['values'][JAF_STYLE_IDENTIFIER]))) {
    form_set_error(JAF_STYLE_IDENTIFIER, t('Invalid style identifier: %identifier.', array('%identifier' => $form_state['values'][JAF_STYLE_IDENTIFIER])));
  }
}




/**
 * Returns the add/edit form
 *
 * @param array &$form_state
 *  A reference to an array which holds form elements
 * @param array $identifier
 *  The optional identifier for the given style, if none is supplied the form
 *  will be in "Add" mode
 * @return boolean
 *  Success indicator
 */
function jaf_admin_form_edit_style($form_state, $identifier = false) {
  $font_array        = array();
  $fonts             = jaf_get_font_list();
  
  if (count($fonts) == 0) {
    drupal_set_message(JAF_MSG_ERROR_NO_FONTS_FOUND, 'error');
    return false;
  }

  // We also use the last font as the default for new styles
  foreach ($fonts as $font) {
    $font_array[$font->getFilename()] = $font->getFullFontName();
  }
  
  $form = array(
    '#type' => 'fieldset',
    JAF_FORM_MODE => array(
      '#type' => 'value',
    ),
    '#submit'        => array('jaf_admin_form_edit_style_submit'),
    '#validate'      => array('jaf_admin_form_edit_style_validate'),
  );

  // Dermine if this is edit or add mode
  if ($identifier === false) {
    // Default values to populate with
    $new_style = array(
      JAF_STYLE_IDENTIFIER => 'Enter a style id here',
      JAF_STYLE_FONT => $font,
      JAF_STYLE_FONT_FILE => $font->getFilename(),
    );
    
    $style = new jaf_Style($new_style);
    
    $form['#title']        = t('Add a new Text image style');
    $form['#description']  = t('Use this form to define a new style and to select which CSS Selectors will be used when searching for text to convert to an image.');
    $form[JAF_FORM_MODE]['#value'] = JAF_FORM_MODE_ADD;
    
  } else {
    $style = jaf_get_style($identifier);

    if (is_null($style)) {
      drupal_set_message(t('
         An invalid identifier (%identifier) was passed or the associated style 
         was not found.', array('%identifier' => $identifier )), 'error');
      return false;
    }
    
    $form['#title']        = t('Edit Text image style: '. $identifier .'.');
    $form['#description']  = t('Make your changes to the style here.');
    $form[JAF_FORM_MODE]['#value'] = JAF_FORM_MODE_EDIT;
  }
  
  $form[JAF_STYLE_IDENTIFIER_PREVIOUS] = array(
    '#type' => 'value',
    '#value' => t($style->getId()),
  );
  $form[JAF_STYLE_IDENTIFIER] = array(
    '#type' => 'textfield',
    '#title' => t('Style Identifier'),
    '#default_value' => t($style->getId()),
    '#description' => t('
      Enter an identifier name, this the name will be used as a handle for the 
      style and will appear in image URIs. Style identifiers may contain letters 
      numbers and underscores.'),
  );
  $form[JAF_STYLE_FONT_FILE] = array(
    '#type' => 'select',
    '#title' => t('Font face'),
    '#default_value' => t($style->font->getFilename()),
    '#options' => $font_array,
    '#description' => t('
      The font that will be used to render images of this Just add fonts style. 
      Two free fonts are pre-packaged with this module the exist in the modules 
      "fonts" directory".'),
  );
  $form[JAF_STYLE_FONT_SIZE] = array(
    '#type' => 'textfield',
    '#title' => t('Font size'),
    '#default_value' => t($style->getFontSize()),
    '#description' => t('The the desired font size messured in points.'),
  );
  $form[JAF_STYLE_FG_COLOR] = array(
    '#type' => 'textfield',
    '#title' => t('Foreground colour'),
    '#default_value' => t($style->getFgColor()),
    '#description' => t('The colour of the text/fontface in HTML hexidecimal format. e.g. #FFCC00.'),
  );
  $form[JAF_STYLE_BG_COLOR] = array(
    '#type' => 'textfield',
    '#title' => t('Background'),
    '#default_value' => t($style->getBgColor()),
    '#description' => t('The colour of the background fill behind the text in HTML hexidecimal format. e.g. #FFCC00.'),
  );
  $form[JAF_STYLE_CSS_SELECTORS] = array(
    '#type' => 'textarea',
    '#title' => t('CSS Selectors'),
    '#default_value' => t($style->getCssSelectors()),
    '#description' => t('This field will accept a simple form of CSS selectors. The CSS selectors will be used to identify which elements will have their text content replaced with an image. This field understands HTML tags (i.e H1, DIV, EM etc), ID Selectors (e.g. #titleHeading, #body etc), Class Selectors (e.g .heading .foobar etc) and Commas (for targeting multiple CSS Selectors) or any valid combination of these selectors (e.g. h1#main.funnyColor, h1#title, div#fig .left etc).'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['cancel'] = array('#value' => l(t('Cancel'), JAF_URI_ADMIN_BASE));
  return $form;
}




/**
 * Handle the form data submit
 *
 * @param array $form
 * @param array $form_state
 */
function jaf_admin_form_edit_style_submit($form, &$form_state) {
  $fonts = jaf_get_font_list();
  $values = $form_state['values'];
    
  foreach ($fonts as $font) {
    if ($font->getFilename() == $values[JAF_STYLE_FONT_FILE]) {
      break;
    }
  }
  
  $values[JAF_STYLE_FONT] = $font;
  
  $styles      = jaf_get_style_list();
  
  $new_style   = new jaf_Style($values);
  
  $old_style   = jaf_get_style($values[JAF_STYLE_IDENTIFIER_PREVIOUS]);
  
  // Do we need to get rid of the old style?
  if (!is_null($old_style)) {
    $old_styles = $styles;
    $styles = array();
    
    // Drop it from the config array
    foreach ($old_styles as $style) {
      if ($style->getId() != $old_style->getId()) {
        array_push($styles, $style);
      }
    }
    
    // Delete the cache directory
    $path = jaf_get_image_path() .'/'. $old_style->getId();
    
    

    // Check if exists first because the cautious rmdir will fail if it doesn't
    if (file_exists($path) && !jaf_do_recursive_rmdir($path)) {
      return false;
    }
  }
  
  array_push($styles, $new_style);
  
  jaf_set_style_list($styles);
  
  // We don't need these anymore, so clean the slate to prevent unwanted 
  // variables from being created and redirect to list view
  $form_state['values'] = array();
  $form_state['redirect'] = JAF_URI_ADMIN_BASE;
  
  jaf_do_create_cache_dirs();
  
  drupal_set_message('New changes have been saved.', 'status');
  
  return true;
}




/**
 * Form validation for jaf_admin_task_edit
 */
function jaf_admin_form_edit_style_validate(&$form, $form_state) {
  $fonts = jaf_get_font_list();
  
  foreach ($fonts as $font) {
    $font_array[$font->getFilename()] = $font->getFullFontName();
  }
  
  // Validate identifier
  if (!preg_match("/^[A-Z0-9_]+$/i", $form_state['values'][JAF_STYLE_IDENTIFIER])) {
    form_set_error(JAF_STYLE_IDENTIFIER, t('Invalid style identifier.'));
  }
  
  $styles = jaf_get_style_list();
  
  foreach ($styles as $style) {
    if ($form_state['values'][JAF_FORM_MODE] == JAF_FORM_MODE_ADD && $style->getId() == $form_state['values'][JAF_STYLE_IDENTIFIER]) {
      form_set_error(JAF_STYLE_IDENTIFIER, t('There is already a style with the identifier: %identifier.', array('%identifier' => $form_state['values'][JAF_STYLE_IDENTIFIER])));
    }
  }

  // Validate font
  if (!in_array($form_state['values'][JAF_STYLE_FONT_FILE], array_keys($font_array))) {
    form_set_error(
      JAF_STYLE_FONT, 
      t('Unknown font "%font".', array('%font' => $form_state['values'][JAF_STYLE_FONT])));
  }

  // Validate font size
  if (!preg_match("/^[0-9]{1,3}$/", $form_state['values'][JAF_STYLE_FONT_SIZE])) {
    form_set_error(
      JAF_STYLE_FONT_SIZE, 
      t('Unsupported font size "%size".', array('%size' => $form_state['values'][JAF_STYLE_FONT_SIZE])));
  }

  // Validate foreground colour
  if (!preg_match("/^\#[A-F0-9]{6}$/i", $form_state['values'][JAF_STYLE_FG_COLOR])) {
    form_set_error(
      JAF_STYLE_FG_COLOR, 
      t('The foreground colour (%color) is not formatted correctly.', array( '%color' => $form_state['values'][JAF_STYLE_FG_COLOR])));
  }

  // Validate background colour
  if (!preg_match("/^\#[A-F0-9]{6}$/i", $form_state['values'][JAF_STYLE_BG_COLOR])) {
    form_set_error(
      JAF_STYLE_BG_COLOR, 
      t('The foreground colour (%color) is not formatted correctly.', array( '%color' => $form_state['values'][JAF_STYLE_BG_COLOR])));
  }
  
  // Validate the css selectors
  // First split them off in to single selectors since there could be multiple selectors.
  $selector_list = split(',', $form_state['values'][JAF_STYLE_CSS_SELECTORS]);
  $invalid_elements = array();
  
  // Pattern match
  $valid_tags = join("|",jaf_get_allowed_html_tags());
  $expression = "/^(($valid_tags))?(\#[a-z0-9-_]+)?(\.[a-z0-9-_]+)?$/i";
  
  
  foreach ($selector_list as $line) {
    // Split each element of a selector
    $elements = split(' ', trim($line));
    
    foreach ($elements as $element) {
      $result = preg_match($expression, $element);
      // Sanity check
     
      if ($result == 0) {
        array_push($invalid_elements, $element);
      }
    }
  }
  
  if (!empty($invalid_elements)) {
    form_set_error(JAF_STYLE_CSS_SELECTORS, t('Problems were detected with the CSS Selectors. Errors were detected for the following CSS selector elements: %invalid.', array('%invalid' => join(', ', $invalid_elements))));
  }
}




/**
 * Provides the form for editing additional settings.
 */
function jaf_admin_form_settings($form_state) {
  $config = jaf_get_configuration(JAF_CONFIG_SETTINGS);
  $form = array(
    '#type'          => 'fieldset',
    '#title'         => 'Settings',
    '#description'   => 'Adjust additional Text image settings',
    '#submit'        => array('jaf_admin_form_settings_submit'),
    '#validate'      => array('jaf_admin_form_settings_validate'),
  );
  $form[JAF_SETTINGS_IMAGE_SUB_URI] = array(
    '#type'          => 'textfield',
    '#title'         => t('Image sub URI'),
    '#default_value' => t($config[JAF_SETTINGS_IMAGE_SUB_URI]),
    '#description'   => t('
      This is the sub-URI which will be appended to the full URI for which 
      images will be accessed with. The sub URI may consist of numbers, letters,
      slashes (/), hyphens (-) and underscores (_). The URI must not start or
      end with a slash.'),
  );
  $form[JAF_SETTINGS_CACHE_ENABLED] = array(
    '#type'          => 'select',
    '#title'         => t('Cache images locally on disk'),
    '#default_value' => t($config[JAF_SETTINGS_CACHE_ENABLED]),
    '#options'       => array('y' => 'Enabled', 'n' => 'Disabled'),
    '#description'   => t('
      This enables the storage of generated images on the server speeding up
      image generation time. If disk caching is enabled, the server is open to
      denial of service attacks by filling up disk space disk. If disk caching 
      is disabled, heavy traffic could slow down the server as it will be 
      generating images for each visit.'),
  );
  $form[JAF_SETTINGS_CACHE_MAX_SIZE] = array(
    '#type'          => 'textfield',
    '#title'         => t('Maximum cache size (bytes)'),
    '#default_value' => t(number_format($config[JAF_SETTINGS_CACHE_MAX_SIZE])),
    '#description'   => t('
      If the "Cache images locally on disk" option is enabled, then this value
      sets the maximum size on disk that the cached images can use. Once the 
      cache limit is reached images will no longer be written to disk and simply
      generated on demand. Numbers and commas (for readability) are okay'),
  );
  $form[JAF_SETTINGS_CACHE_SUB_PATH] = array(
    '#type'          => 'textfield',
    '#title'         => t('Cache sub path'),
    '#default_value' => t($config[JAF_SETTINGS_CACHE_SUB_PATH]),
    '#description'   => t('
      This is the sub path which exists under the currently configured drupal 
      "file directory path". When a image is created to replace some text, the 
      file will be stored under this sub path. The sub path may consist of 
      numbers, letters, slashes (/), hyphens (-) and underscores (_). The path 
      must not start or end with a slash.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['cancel'] = array('#value' => l(t('Cancel'), JAF_URI_ADMIN_BASE));
  return $form;
}




function jaf_admin_form_settings_submit(&$form, &$form_state) {
  $values = array();
  
  // Collect the key value pair from form state that we wish to save
  foreach ($form_state['values'] as $key => $value) {
    switch ($key) {
      case JAF_SETTINGS_CACHE_MAX_SIZE: // fall through
        // Remove the commas
        $value = join('', split(',',$value));
      case JAF_SETTINGS_IMAGE_SUB_URI: // fall through
      case JAF_SETTINGS_CACHE_ENABLED: // fall through
      case JAF_SETTINGS_CACHE_SUB_PATH:
        $values[$key] = $value;
      break;
    }
  }
  
  jaf_set_configuration(JAF_CONFIG_SETTINGS, $values);
  
  // We don't need these anymore, so clean the slate to prevent unwanted 
  // variables from being created and redirect to list view
  $form_state['values'] = array();
  $form_state['redirect'] = JAF_URI_ADMIN_BASE;
  
  drupal_set_message('New settings have been saved.', 'status');
  
  return true;
}




function jaf_admin_form_settings_validate(&$form, $form_state) {

  // Validate sub uri
  if (!preg_match("/^[^\/](\/?[a-z0-9_-]+)+[^\/]$/i", $form_state['values'][JAF_SETTINGS_IMAGE_SUB_URI])) {
    form_set_error(
      JAF_SETTINGS_IMAGE_SUB_URI, 
      t('Invalid sub URI.'));
  }
  
  // Validate cache enabled
  if (!preg_match("/^(Y|N)$/i", $form_state['values'][JAF_SETTINGS_CACHE_ENABLED])) {
    form_set_error(
      JAF_SETTINGS_CACHE_ENABLED, 
      t('Invalid selection detected for the cache enabled option.'));
  }
  
  // Validate cache max size
  if (!preg_match("/^[0-9]{0,3}(,?[0-9]{3})*$/i", $form_state['values'][JAF_SETTINGS_CACHE_MAX_SIZE])) {
    form_set_error(
      JAF_SETTINGS_CACHE_MAX_SIZE, 
      t('Invalid option for cache max size.'));
  }
  
  // Validate cache enabled
  if (!preg_match("/^[^\/](\/?[a-z0-9_-]+)+[^\/]$/i", $form_state['values'][JAF_SETTINGS_CACHE_SUB_PATH])) {
    form_set_error(
      JAF_SETTINGS_CACHE_SUB_PATH, 
      t('Invalid option for cache sub path.'));
  }
}




/**
 * Formats the given form array for the list styles page
 *
 * @param array $fonts
 *  An associative array of font names and titles
 * @return string
 *  Tabulated and HTML formated Text image styles
 */
function jaf_admin_view_styles() {
  $styles = jaf_get_style_list();
  $rows = array();

  $header = array(
    array('data' => t('Identifier'),      'field' => JAF_STYLE_IDENTIFIER, 'sort' => 'asc'),
    array('data' => t('Font'),            'field' => JAF_STYLE_FONT),
    array('data' => t('Size (pt)'),       'field' => JAF_STYLE_FONT_SIZE),
    array('data' => t('Foreground'),      'field' => JAF_STYLE_FG_COLOR),
    array('data' => t('Background'),      'field' => JAF_STYLE_BG_COLOR),
    array('data' => t('CSS Selector(s)'), 'field' => JAF_STYLE_CSS_SELECTORS),
    array('data' => t('Operations'),      'colspan' => 2),
  );

  foreach ($styles as $style) {
    $id = $style->getId();

    // Just in-case the font wasn't found
    $font = is_null($style->font) ? $style->getFontFile() : $style->font->getFullFontName() ;

    $row = array(
      'data' => array(
        $id,
        $font,
        $style->getFontSize(),
        $style->getFgColor(),
        $style->getBgColor(),
        $style->getCssSelectors(),
        l(t('edit'),     JAF_URI_ADMIN_STYLE_EDIT  .'/' . $id),
        l(t('delete'),   JAF_URI_ADMIN_STYLE_DELETE  .'/' . $id),
      )
    );

    array_push($rows, $row);
  }


  if (empty($rows)) {
    array_push($rows, array(array('data' => t('There are no styles yet. !link.', array('!link' => l(t('You can add a style here'), JAF_URI_ADMIN_STYLE_ADD))), 'colspan' => '8')));
  }

  $output = theme('table', $header, $rows, array('class' => 'jaf'));

  return $output;
}
