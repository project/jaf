
This module provides dynamic substitution of text with dynamically generated 
images using True type fonts.

Each time a user views a page, pre-defined CSS selectors are sought for and 
their text is replaced with an image containing the text. The text is rendered 
according to a predefined "pseudo style" comprised of: foreground colour, 
background colour, font size and font face.

The key feature of this module is the use of your own True type fonts. This 
allows organizations who might wish to use customized fonts for their headings, 
but the huge number of pages would make this a time consuming task to generate 
each of the images required for each page.

The substitution is done during page load by leveraging the built in Drupal 
JQuery library, and aims to be as usability compatible as possible for things 
like screen readers.

Installation
------------
1. Extract the module archive to your Drupal modules directory.
2. Copy some True type font files to this module's fonts sub directory. This is
usually modules/jaf/fonts
3. Enable the module in Drupal

Post Installation
-----------------
Once the module is enabled and has some TTF fonts installed, you will be able to
configure it from the menu: Administration -> Site building ->  Just add fonts
or via the URI /admin/build/jaf.

For more information and documentation please see the project page, the URL is:
http://drupal.org/project/jaf

Author
------
Aaron Chilcott (azman)
http://drupal.org/user/505736
